package org.example;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

// This annotation is for the classes
// that can be serialized to a JSON string

@Retention(RUNTIME) //  has runtime visibility
@Target(TYPE) // can be applied to types (classes)
public @interface JsonSerializable {

}
