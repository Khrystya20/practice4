package org.example;

import java.lang.annotation.*;

// This annotation is for the setter methods
// It allows to generate the Builder class for each class
// that has its setter methods annotated
@Target(ElementType.METHOD) // this annotation can be only put on a method
// this annotation is only available during source processing and is not available at runtime
@Retention(RetentionPolicy.SOURCE)
public @interface BuilderProperty {
}