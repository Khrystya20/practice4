package org.example;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

// This annotation is for the fields of object
// that should be contained in JSON string

@Retention(RUNTIME) //  has runtime visibility
@Target({ FIELD }) // can be applied to fields
public @interface JsonElement {
    public String key() default "";
}
