package org.example;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// This annotation is for the classes that should be included
// in the generated class to order classes with the specific methods

@Target(ElementType.TYPE) // can be applied to types (classes)
@Retention(RetentionPolicy.CLASS)
public @interface WorkWithProduct {
    Class type(); // The name of the factory
    String id(); // The identifier for determining which item should be instantiated
}
