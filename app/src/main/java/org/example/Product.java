package org.example;

@JsonSerializable // can serialize a Product object to a JSON string
public class Product {

    // key - the identifier for the field in the JSON output
    @JsonElement(key = "productName")
    private String name;
    @JsonElement(key = "productDescription")
    private String description;
    @JsonElement(key = "productPrice")
    private double price;

    public Product(){}

    public Product(String name, String description, double price){
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    @BuilderProperty
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    @BuilderProperty
    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    @BuilderProperty
    public void setPrice(double price) {
        this.price = price;
    }

}
