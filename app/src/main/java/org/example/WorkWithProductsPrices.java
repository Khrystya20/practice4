package org.example;

import java.util.List;

@WorkWithProduct(
        id="WorkWithPrices",
        type=WorkWithProducts.class
)
public class WorkWithProductsPrices implements WorkWithProducts{

    public void increasePriceByPercent(List<Product> products, double percent){
        for(Product product : products){
            increasePriceByPercent(product, percent);
        }
    }

    public void increasePriceByPercent(Product product, double percent){
        double increasedPrice = product.getPrice() * (100 + percent) / 100;
        product.setPrice(increasedPrice);
    }

    public void decreasePriceByPercent(List<Product> products, double percent){
        for(Product product : products){
            decreasePriceByPercent(product, percent);
        }
    }

    public void decreasePriceByPercent(Product product, double percent){
        double decreasedPrice = product.getPrice() * (100 - percent) / 100;
        product.setPrice(decreasedPrice);
    }

    public String getClassName(){
        return "WorkWithProductsPrices";
    }

}
