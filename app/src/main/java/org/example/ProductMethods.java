package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ProductMethods {

    private final WorkWithProductsGenerated methodsWithProducts = new WorkWithProductsGenerated();

    public WorkWithProducts order(String methodName){
        return methodsWithProducts.create(methodName);
    }

    private static String readConsole() throws IOException {
        System.out.println("What class with product methods do you choose?");
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        String input = bufferRead.readLine();
        return input;
    }

    public static void main(String[] args) throws IOException {
        ProductMethods productMethods = new ProductMethods();
        WorkWithProducts workWithProducts = productMethods.order(readConsole());
        System.out.println("You ordered the class: " + workWithProducts.getClassName());
    }
}
