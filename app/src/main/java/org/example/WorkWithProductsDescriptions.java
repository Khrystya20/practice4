package org.example;

import java.util.List;

@WorkWithProduct(
        id="WorkWithDescriptions",
        type=WorkWithProducts.class
)
public class WorkWithProductsDescriptions implements WorkWithProducts{

    public void changeProductsDescription(List<Product> products, String phraseToAdd){
        for(Product product : products){
            product.setDescription(product.getDescription() + phraseToAdd);
        }
    }

    public String getClassName(){
        return "WorkWithProductsDescriptions";
    }

}
