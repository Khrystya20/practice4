package org.example;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CreateProductByBuilderUnitTest {

    /* Test ProductBuilder class, its methods */
    @Test
    public void createProductByBuilder(){
        // ProductBuilder - a builder helper class to instantiate the Product class more fluently
        Product product = new ProductBuilder()
                .setName("apple")
                .setDescription("green apple")
                .setPrice(34.9)
                .build();
        ObjectToJsonConverter serializer = new ObjectToJsonConverter();
        String jsonString = serializer.convertToJson(product);
        //System.out.println(jsonString);
        assertEquals("{\"productName\":\"apple\",\"productDescription\":\"green apple\",\"productPrice\":\"34.9\"}", jsonString);
    }
}
