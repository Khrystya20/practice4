package org.example;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class ObjectToJsonConverter {
    public String convertToJson(Object object) throws JsonSerializationException {
        try {
            checkIfSerializable(object);
            return getJsonString(object);
        } catch (Exception e) {
            throw new JsonSerializationException(e.getMessage());
        }
    }

    private void checkIfSerializable(Object object) {
        // Check whether the object is null or not
        if (Objects.isNull(object)) {
            throw new JsonSerializationException("Can't serialize a null object");
        }
        Class<?> clazz = object.getClass();
        // Check whether object's type has the @JsonSerializable annotation or not
        if (!clazz.isAnnotationPresent(JsonSerializable.class)) {
            throw new JsonSerializationException("The class " + clazz.getSimpleName() + " is not annotated with JsonSerializable");
        }
    }

    private String getJsonString(Object object) throws IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = object.getClass();
        Map<String, Object> jsonElementsMap = new HashMap<>();
        // Iterate over object's fields
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(JsonElement.class)) {
                // Put the key and value in a map
                jsonElementsMap.put(getKey(field), field.get(object));
            }
        }
        // Check whether field's values are valid
        if (!areFieldsValid(jsonElementsMap)) {
            throw new JsonSerializationException("The object has incorrect fields values");
        }
        // Create the JSON string from the map
        String jsonString = jsonElementsMap.entrySet()
                .stream()
                .map(entry -> "\"" + entry.getKey() + "\":\"" + entry.getValue().toString() + "\"")
                .collect(Collectors.joining(","));
        return "{" + jsonString + "}";
    }

    private String getKey(Field field) {
        String value = field.getAnnotation(JsonElement.class)
                .key();
        return value.isEmpty() ? field.getName() : value;
    }

    private boolean areFieldsValid(Map<String, Object> jsonElementsMap){
        for (Map.Entry<String, Object> entry : jsonElementsMap.entrySet()) {
            if(Objects.equals(entry.getKey(), "productName") && entry.getValue().toString().length() > 50){
               return false;
            }
           if(Objects.equals(entry.getKey(), "productPrice") &&  (Double)entry.getValue() <= 0){
               return false;
           }
        }
        return true;
    }

}