package org.example;

import com.google.auto.service.AutoService;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.ExecutableType;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@SupportedAnnotationTypes("org.example.BuilderProperty")
@SupportedSourceVersion(SourceVersion.RELEASE_11)
// This annotation processor from the auto-service library generates
// the META-INF/services/javax.annotation.processing.Processor file containing the BuilderProcessor class name.
@AutoService(Processor.class)
// BuilderProcessor is a simple processor for generating fluent object builders for annotated classes
public class BuilderProcessor extends AbstractProcessor {
    // Method "process" is called by the compiler for every source file containing the matching annotations
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        // Set<? extends TypeElement> annotations argument is set of annotations
        // RoundEnviroment roundEnv argument has the information about the current processing round
        for (TypeElement annotation : annotations) {
            // Get elements annotated with current annotation
            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            // Split annotated methods into two collections:
            // correctly annotated setters and other erroneously annotated methods
            Map<Boolean, List<Element>> annotatedMethods = annotatedElements.stream().collect(Collectors.partitioningBy(element -> ((ExecutableType) element.asType()).getParameterTypes().size() == 1 && element.getSimpleName().toString().startsWith("set")));
            List<Element> setters = annotatedMethods.get(true);
            List<Element> otherMethods = annotatedMethods.get(false);
            // Output an error for each erroneously annotated element during the source processing stage
            otherMethods.forEach(element -> processingEnv.getMessager().printMessage(Diagnostic.Kind.ERROR, "@BuilderProperty must be applied to a setXxx method with a single argument", element));
            // If the correct setters collection is empty, there is no point of continuing the current type element set iteration
            if (setters.isEmpty()) {
                continue;
            }
            // Get name of the class, where setters have annotation BuilderProperty
            String className = ((TypeElement) setters.get(0).getEnclosingElement()).getQualifiedName().toString();
            // This is map between the names of the setters and the names of their argument types
            Map<String, String> setterMap = setters.stream().collect(Collectors.toMap(setter -> setter.getSimpleName().toString(), setter -> ((ExecutableType) setter.asType()).getParameterTypes().get(0).toString()));
            try {
                writeBuilderFile(className, setterMap);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return true;
    }

    private void writeBuilderFile(String className, Map<String, String> setterMap) throws IOException {
        // Set the package name, fully qualified builder class name,
        // and simple class names for the source class and the builder class
        String packageName = null;
        int lastDot = className.lastIndexOf('.');
        if (lastDot > 0) {
            packageName = className.substring(0, lastDot);
        }
        String simpleClassName = className.substring(lastDot + 1);
        String builderClassName = className + "Builder";
        String builderSimpleClassName = builderClassName.substring(lastDot + 1);
        // Generate the output file using Filer
        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(builderClassName);
        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
            if (packageName != null) {
                out.print("package ");
                out.print(packageName);
                out.println(";");
                out.println();
            }
            out.print("public class ");
            out.print(builderSimpleClassName);
            out.println(" {");
            out.println();
            out.print("    private ");
            out.print(simpleClassName);
            out.print(" object = new ");
            out.print(simpleClassName);
            out.println("();");
            out.println();
            out.print("    public ");
            out.print(simpleClassName);
            out.println(" build() {");
            out.println("        return object;");
            out.println("    }");
            out.println();
            setterMap.forEach((methodName, argumentType) -> {
                out.print("    public ");
                out.print(builderSimpleClassName);
                out.print(" ");
                out.print(methodName);
                out.print("(");
                out.print(argumentType);
                out.println(" value) {");
                out.print("        object.");
                out.print(methodName);
                out.println("(value);");
                out.println("        return this;");
                out.println("    }");
                out.println();
            });
            out.println("}");
        }
    }

}