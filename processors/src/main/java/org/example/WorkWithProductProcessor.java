package org.example;

import com.google.auto.service.AutoService;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@AutoService(Processor.class) // Annotation processor generates the META-INF/services/javax.annotation.processing.Processor file
public class WorkWithProductProcessor extends AbstractProcessor {

    private Types typeUtils; // work with TypeMirror
    private Elements elementUtils; // work with Element classes
    private Filer filer; // create files
    private Messager messager; // report error messages, warnings and other notices
    private Map<String, WorkGroupedClasses> workWithProductClasses = new LinkedHashMap<String, WorkGroupedClasses>();

    @Override
    public synchronized void init(ProcessingEnvironment processingEnv) {
        super.init(processingEnv);
        typeUtils = processingEnv.getTypeUtils();
        elementUtils = processingEnv.getElementUtils();
        filer = processingEnv.getFiler();
        messager = processingEnv.getMessager();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        // Specify for which annotations this annotation processor should be registered for
        Set<String> annotations = new LinkedHashSet<String>();
        annotations.add(WorkWithProduct.class.getCanonicalName());
        return annotations;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        // Specify java version
        return SourceVersion.latestSupported();
    }

    /**
     * Checks if the annotated element observes some rules
     */
    private void checkValidClass(WorkAnnotatedClass item) throws ProcessingException {
        // Cast to TypeElement, has more type specific methods
        TypeElement classElement = item.getTypeElement();
        // The class must be public
        if (!classElement.getModifiers().contains(Modifier.PUBLIC)) {
            throw new ProcessingException(classElement, "The class %s is not public.",
                    classElement.getQualifiedName().toString());
        }
        // Check if it's an abstract class
        // The class can not be abstract
        if (classElement.getModifiers().contains(Modifier.ABSTRACT)) {
            throw new ProcessingException(classElement,
                    "The class %s is abstract. You can't annotate abstract classes with @%",
                    classElement.getQualifiedName().toString(), WorkWithProduct.class.getSimpleName());
        }
        // The class must be subclass or implement the Class as specified in @WorkWithProduct.type().
        // Check inheritance: Class must be childclass as specified in @WorkWithProduct.type();
        TypeElement superClassElement =
                elementUtils.getTypeElement(item.getQualifiedGroupName());
        if (superClassElement.getKind() == ElementKind.INTERFACE) {
            // Check interface implemented
            if (!classElement.getInterfaces().contains(superClassElement.asType())) {
                throw new ProcessingException(classElement,
                        "The class %s annotated with @%s must implement the interface %s",
                        classElement.getQualifiedName().toString(), WorkWithProduct.class.getSimpleName(),
                        item.getQualifiedGroupName());
            }
        } else {
            // Check subclassing
            TypeElement currentClass = classElement;
            while (true) {
                TypeMirror superClassType = currentClass.getSuperclass();
                if (superClassType.getKind() == TypeKind.NONE) {
                    // Basis class (java.lang.Object) reached, so exit
                    throw new ProcessingException(classElement,
                            "The class %s annotated with @%s must inherit from %s",
                            classElement.getQualifiedName().toString(), WorkWithProduct.class.getSimpleName(),
                            item.getQualifiedGroupName());
                }
                if (superClassType.toString().equals(item.getQualifiedGroupName())) {
                    // Required super class found
                    break;
                }
                // Moving up in inheritance tree
                currentClass = (TypeElement) typeUtils.asElement(superClassType);
            }
        }
        // Check if an empty public constructor is given
        for (Element enclosed : classElement.getEnclosedElements()) {
            if (enclosed.getKind() == ElementKind.CONSTRUCTOR) {
                ExecutableElement constructorElement = (ExecutableElement) enclosed;
                if (constructorElement.getParameters().size() == 0 && constructorElement.getModifiers()
                        .contains(Modifier.PUBLIC)) {
                    // Found an empty constructor
                    return;
                }
            }
        }
        // No empty constructor found
        throw new ProcessingException(classElement,
                "The class %s must provide an public empty default constructor",
                classElement.getQualifiedName().toString());
    }

    // Scanning, evaluating and processing annotations and generating java files
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        try {
            // Iterate over all @WorkWithProduct annotated elements (element can be a class, method, variable etc)
            for (Element annotatedElement : roundEnv.getElementsAnnotatedWith(WorkWithProduct.class)) {
                // Check if a class has been annotated with @WorkWithProduct
                if (annotatedElement.getKind() != ElementKind.CLASS) {
                    throw new ProcessingException(annotatedElement, "Only classes can be annotated with @%s",
                            WorkWithProduct.class.getSimpleName());
                }
                // We can cast it, because we know that it of ElementKind.CLASS
                TypeElement typeElement = (TypeElement) annotatedElement;
                // All classes annotated with @WorkWithProduct will be stored as annotatedClass and grouped into WorkGroupedClasses
                WorkAnnotatedClass annotatedClass = new WorkAnnotatedClass(typeElement);
                checkValidClass(annotatedClass);
                // Everything is fine, so try to add
                WorkGroupedClasses workWithProductClass =
                        workWithProductClasses.get(annotatedClass.getQualifiedGroupName());
                if (workWithProductClass == null) {
                    String qualifiedGroupName = annotatedClass.getQualifiedGroupName();
                    workWithProductClass = new WorkGroupedClasses(qualifiedGroupName);
                    workWithProductClasses.put(qualifiedGroupName, workWithProductClass);
                }
                // Checks if id is conflicting with another @WorkWithProduct annotated class with the same id
                workWithProductClass.add(annotatedClass);
            }
            // Generate code
            // Generate java files for each WorkGroupedClasses
            for (WorkGroupedClasses workWithProductClass : workWithProductClasses.values()) {
                workWithProductClass.generateCode(elementUtils, filer);
            }
            workWithProductClasses.clear();
        } catch (ProcessingException e) {
            error(e.getElement(), e.getMessage());
        } catch (IOException e) {
            error(null, e.getMessage());
        }

        return true;
    }

    /**
     * Prints an error message
     *
     * @param e The element which has caused the error. Can be null
     * @param msg The error message
     */
    public void error(Element e, String msg) {
        messager.printMessage(Diagnostic.Kind.ERROR, msg, e);
    }

}
