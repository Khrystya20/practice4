package org.example;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.MirroredTypeException;

import org.apache.commons.lang3.StringUtils;

public class WorkAnnotatedClass {

    private TypeElement annotatedClassElement;
    private String qualifiedGroupClassName;
    private String simpleGroupName;
    private String id;

    /**
     * @throws ProcessingException if id() from annotation is null
     */
    public WorkAnnotatedClass(TypeElement classElement) throws ProcessingException {
        this.annotatedClassElement = classElement;
        WorkWithProduct annotation = classElement.getAnnotation(WorkWithProduct.class);
        id = annotation.id();

        if (StringUtils.isEmpty(id)) {
            throw new ProcessingException(classElement,
                    "id() in @%s for class %s is null or empty! that's not allowed",
                    WorkWithProduct.class.getSimpleName(), classElement.getQualifiedName().toString());
        }

        // Get the full QualifiedTypeName
        try {
            Class<?> clazz = annotation.type();
            qualifiedGroupClassName = clazz.getCanonicalName();
            simpleGroupName = clazz.getSimpleName();
        } catch (MirroredTypeException mte) {
            DeclaredType classTypeMirror = (DeclaredType) mte.getTypeMirror();
            TypeElement classTypeElement = (TypeElement) classTypeMirror.asElement();
            qualifiedGroupClassName = classTypeElement.getQualifiedName().toString();
            simpleGroupName = classTypeElement.getSimpleName().toString();
        }
    }

    /**
     * Get the id as specified in {@link WorkWithProduct#id()}.
     * return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Get the full qualified name of the type specified in  {@link WorkWithProduct#type()}.
     *
     * @return qualified name
     */
    public String getQualifiedGroupName() {
        return qualifiedGroupClassName;
    }

    /**
     * Get the simple name of the type specified in  {@link WorkWithProduct#type()}.
     *
     * @return qualified name
     */
    public String getSimpleGroupName() {
        return simpleGroupName;
    }

    /**
     * The original element that was annotated with @WorkWithProduct
     */
    public TypeElement getTypeElement() {
        return annotatedClassElement;
    }



}
